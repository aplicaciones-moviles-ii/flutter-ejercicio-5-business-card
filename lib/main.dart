import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  final color = Colors.purple;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: color,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const CircleAvatar(
                radius: 100.0,
                backgroundImage: NetworkImage('http://placekitten.com/450/450'),
              ),
              const SizedBox(height: 15.0),
              const Text(
                'Triki',
                style: TextStyle(
                  fontSize: 35.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -1.0,
                ),
              ),
              const SizedBox(height: 10.0),
              Text(
                'AwEsOmE kIttY'.toLowerCase(),
                style: TextStyle(
                  fontSize: 24.0,
                  color: Colors.white.withOpacity(0.6),
                ),
              ),
              SizedBox(
                height: 40.0,
                width: 120.0,
                child: Divider(
                  color: Colors.white.withOpacity(0.6),
                ),
              ),
              infoCard(
                icon: Icons.favorite,
                text: 'I love tuna, ham and milk',
              ),
              infoCard(
                icon: Icons.people,
                text: 'I own two people',
              ),
              infoCard(
                icon: Icons.date_range,
                text: 'I am 2 months old',
              )
            ],
          ),
        ),
      ),
    );
  }

  // We are going to use this widget many times, so it makes sense to extract it to
  // avoid code repetition. In fact, even if we are only going to use it once, it may
  // be a good idea to extract widgets into their own functions or classes.
  Widget infoCard({required IconData icon, required String text}) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          leading: Icon(
            icon,
            size: 30.0,
            color: color.shade400,
          ),
          title: Text(
            text,
            style: TextStyle(
              fontSize: 18.0,
              color: color.shade400,
            ),
          ),
        ),
      ),
    );
  }
}
